Cara menjalankan project ini
1. Setelah clone project ini, copy isi file .env.example ke .env
2. Buat database SQL dengan nama sesuai yang tertera di file.env -> DB_DATABASE=fm_product
3. Migrate database blueprint dengan perintah php artisan migrate
4. Kirimkan seed dengan perintah php artisan db:seed
5. Runing project dengan menjalakan perintah php artisan serve --host=0.0.0.0 --port=8001
7. Setelah runing API bisa digunakan oleh frontend