<?php

namespace Database\Seeders;
use App\Models\Categories;
use App\Models\Products;
use App\Models\Images;
use App\Models\Colors;
use App\Models\Sizes;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Categories::create([
            'name' =>'Pakaian'
        ]);
        Categories::create([
            'name' =>'Celana'
        ]);
        Categories::create([
            'name' =>'Topi'
        ]);
        Categories::create([
            'name' =>'Jas'
        ]);
        Categories::create([
            'name' =>'Sepatu'
        ]);
        Categories::create([
            'name' =>'Jaket'
        ]);
        Categories::create([
            'name' =>'Tas'
        ]);
        Categories::create([
            'name' =>'Dompet'
        ]);
        Categories::create([
            'name' =>'Lainnya'
        ]);

        Products::create([
            'name' =>'Kaos Polos',
            'category_id' =>'1',
            'description' =>'Kaos polos dengan bahan yang nyaman'
        ]);
        Products::create([
            'name' =>'Celana skiny',
            'category_id' =>'2',
            'description' =>'Celana panjang skiny dengan bahan yang mudah beradaptasi'
        ]);
        Products::create([
            'name' =>'Topi NY',
            'category_id' =>'3',
            'description' =>'Topi NY import dari USA'
        ]);
        Products::create([
            'name' =>'Sepatu Nike Air',
            'category_id' =>'5',
            'description' =>'Sepatu nike dengan design keren, bagus digunakan pada saat lari dan jalan - jalan'
        ]);
        Colors::create([
            'name' =>'Merah',
            'product_id' =>'1'
        ]);
        Colors::create([
            'name' =>'Biru',
            'product_id' =>'1'
        ]);
        Colors::create([
            'name' =>'Biru Navy',
            'product_id' =>'2'
        ]);
        Colors::create([
            'name' =>'Hitam',
            'product_id' =>'3'
        ]);
        Colors::create([
            'name' =>'Putih',
            'product_id' =>'3'
        ]);
        Colors::create([
            'name' =>'Merah',
            'product_id' =>'3'
        ]);
        Colors::create([
            'name' =>'Hitam Putih',
            'product_id' =>'4'
        ]);
        Sizes::create([
            'size' =>'S',
            'price' =>'50000',
            'product_id' =>'1'
        ]);
        Sizes::create([
            'size' =>'M',
            'price' =>'55000',
            'product_id' =>'1'
        ]);
        Sizes::create([
            'size' =>'L',
            'price' =>'60000',
            'product_id' =>'1'
        ]);
        Sizes::create([
            'size' =>'All Size',
            'price' =>'150000',
            'product_id' =>'2'
        ]);
        Sizes::create([
            'size' =>'All Size',
            'price' =>'30000',
            'product_id' =>'3'
        ]);
        Sizes::create([
            'size' =>'All Size',
            'price' =>'500000',
            'product_id' =>'4'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395511-487c510c3c99360c2dea2f016bb2d0f6.jpg',
            'product_id' =>'1'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395525-bb6d37ecb2438122da6b1d3d7da8da9b.jpg',
            'product_id' =>'1'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395531-a012ea3c72973bc4d785223a7b0cfb28.jpg',
            'product_id' =>'1'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395591-015adb5866027e98cb76e22421701689.jpg',
            'product_id' =>'2'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395615-8d1d70adcbfe0d5125512597c5bac6ca.jpg',
            'product_id' =>'3'
        ]);
        Images::create([
            'image' =>'laravel1/productimage/1646395629-2d0f04879fe838b09245bffb36979a12.jpg',
            'product_id' =>'4'
        ]);
    }
}
