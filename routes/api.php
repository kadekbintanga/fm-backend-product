<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('showallproducts', 'ApiController@showAllProductsDetail')->name('showallproducts');
Route::get('showlistproducts', 'ApiController@showListProducts')->name('showlistproducts');
Route::get('showproduct/{id}', 'ApiController@showProductDetail')->name('showproduct');
Route::get('searchproduct', 'ApiController@searchProduct')->name('searchproduct');
Route::post('add/product', 'ApiController@addProduct')->name('add.product');
Route::post('add/size', 'ApiController@addSize')->name('add.size');
Route::post('add/color', 'ApiController@addColor')->name('add.color');
Route::post('add/image', 'ApiController@uploadImage')->name('add.image');
Route::post('deleteproduct', 'ApiController@deleteProduct')->name('deleteproduct');