<?php

function uploadImage($files, $targetProject, $targetFolder) {

    $targetPath = public_path($targetProject);
    $fileExt = $files->getClientOriginalExtension();
    $originalName = $files->getClientOriginalName();
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
    $newfilename = $timestamp."-".md5($originalName).".".$fileExt;
    try {
        $file = $files->storeAs($targetFolder, $newfilename);
        $response = $targetProject.'/'.$targetFolder.'/'.$newfilename;
        error_log("Giving Response: $response from: ".storage_path('app/'.$file));
        File::move(storage_path('app/'.$file), $targetPath.'/'.$targetFolder.'/'.$newfilename);
        $callback['status'] = 200;
        $callback['response'] = $response;
        return $callback;
    } catch (\Exception $ex) {
        $callback['status'] = 422;
        $callback['response'] = $ex;
        return $callback;
    }
}
