<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'image','product_id'];
    public function product_id() {
        return $this->belongsToManny(Products::class);
    }
}
