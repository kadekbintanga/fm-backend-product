<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sizes extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'size', 'price','product_id'];

    public function product_id(){
        return $this->belongsToMany(Products::class);
    }
}
