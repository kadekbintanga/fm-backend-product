<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'category_id', 'name', 'description'];
    
    public function colors() {
        return $this->hasMany(Colors::class,"product_id");
    }

    public function sizes(){
        return $this->hasMany(Sizes::class,"product_id");
    }

    public function images(){
        return $this->hasMany(Images::class,"product_id");
    }

    public function lowestprice(){
        return $this->hasOne(Sizes::class,"product_id")->ofMany('price','min');
    }

    public function highestprice(){
        return $this->hasOne(Sizes::class,"product_id")->ofMany('price','max');
    }

    public function latestimage(){
        return $this->hasOne(Images::class,"product_id")->latestOfMany();
    }

    public function getAllProductsDetail() {
        return Products::select('products.id', 'products.name as name', 'categories.name as category', 'description')
            ->leftJoin('categories', 'products.category_id','=', 'categories.id')
            ->with('colors', 'sizes','images')
            ->get();
    }

    public function getListProducts(){
        return Products::select('products.id', 'products.name as name','categories.name as category')
            ->leftJoin('categories', 'products.category_id','=', 'categories.id')
            ->with('lowestprice', 'highestprice', 'latestimage')
            ->get();
    }
    public function getProduct($id){
        return Products::where('products.id',$id)
            ->select('products.id', 'products.name as name', 'categories.name as category', 'description')
            ->leftJoin('categories', 'products.category_id','=', 'categories.id')
            ->with('colors', 'sizes','images')
            ->first();
    }
    public function searchProduct($category, $keyword){
        $query =  Products::select('products.id', 'products.name as name','categories.name as category')
            ->leftJoin('categories', 'products.category_id','=', 'categories.id');
        if (!empty($category)){ 
            $query->where('categories.name', $category);
        }
        if (!empty($keyword)){
            $query->where('products.name','like', '%'.$keyword.'%');
        }
        $query->with('lowestprice', 'highestprice', 'latestimage');
        return $query->get();
    }
}
