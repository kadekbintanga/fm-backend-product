<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Images;
use App\Models\Colors;
use App\Models\Sizes;
use Illuminate\Http\Request;
use Validator;

class ApiController extends Controller
{
    public function showAllProductsDetail(Request $request){
        $response = new Products();
        $data = $response->getAllProductsDetail();
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }
    public function showListProducts(Request $request){
        $response = new Products();
        $data = $response->getListProducts();
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }
    public function showProductDetail(Request $request, $id){
        $response = new Products();
        $data = $response->getProduct($id);
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }
    public function searchProduct(Request $request){
        $category = $request->get("category");
        $keyword = $request->get("keyword");
        $response = new Products();
        $data = $response->searchProduct($category, $keyword);
        return response()->json(['status'=> 'success', 'data' => $data], 200);
    }
    public function addProduct(Request $request){
        $product_category = $request->get("product_category");
        $product_name = $request->get("product_name");
        $product_description = $request->get("product_description");
        $product = new Products();
        $product->category_id = $product_category;
        $product->name = $product_name;
        $product->description = $product_description;
        $product->save();
        $productid = $product->id;
        return response()->json(['status'=> 'success', 'id' => $productid], 201);
    }
    public function addSize(Request $request){
        $product_size = $request->get("product_size");
        $product_price = $request->get("product_price");
        $product_id = $request->get("product_id");
        $product = new Sizes();
        $product->size = $product_size;
        $product->price = $product_price;
        $product->product_id = $product_id;
        $product->save();
        return response()->json(['status'=> 'success'], 201);
    }
    public function addColor(Request $request){
        $product_color = $request->get("product_color");
        $product_id = $request->get("product_id");
        $product = new Colors();
        $product->name = $product_color;
        $product->product_id = $product_id;
        $product->save();
        return response()->json(['status'=> 'success'], 201);
    }
    public function uploadImage(Request $request) {
        $product_id = $request->get("product_id");
        $targetProject = 'laravel1';
        $targetFolder = 'productimage';
        $validator = Validator::make($request->all(),
            [
                'photo' => 'required|mimes:png,jpg|max:2048',
            ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $callback = uploadImage($request->file('photo'), $targetProject, $targetFolder);
        if($callback["status"] == 200) {
            $product = new Images();
            $product->image = $callback['response'];
            $product->product_id = $product_id;
            $product->save();
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "imagepath" => $callback['response']
            ]);
        } else {
            return response()->json(['error'=> "Photo gagal di proses, pastikan besar photo kurang dari 2Mb - ".$callback['response']], 422);
        }
    }
    public function deleteProduct(Request $request){
        $id = $request->get("id");
        Products::where('id',$id)->delete();
        Colors::where('product_id',$id)->delete();
        Sizes::where('product_id',$id)->delete();
        Images::where('product_id',$id)->delete();
        return response()->json(['status'=> 'success'],200);
    }
}
